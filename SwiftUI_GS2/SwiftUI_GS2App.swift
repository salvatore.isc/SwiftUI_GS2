//
//  SwiftUI_GS2App.swift
//  SwiftUI_GS2
//
//  Created by Salvador Lopez on 09/06/23.
//

import SwiftUI

@main
struct SwiftUI_GS2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
