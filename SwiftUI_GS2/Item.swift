//
//  Item.swift
//  SwiftUI_GS2
//
//  Created by Salvador Lopez on 09/06/23.
//

import Foundation

struct Item: Identifiable {
    var id = UUID()
    let name: String
    let description: String
}
