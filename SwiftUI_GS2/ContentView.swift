//
//  ContentView.swift
//  SwiftUI_GS2
//
//  Created by Salvador Lopez on 09/06/23.
//

import SwiftUI

struct ContentView: View {
    @State var userText: String = ""
    @State var userPassText: String = ""
    @State var estadoToggle: Bool = false
    
    var body: some View {
        VStack{
            Text("¡Welcome!")
                .font(.largeTitle)
                .fontWeight(.ultraLight)
                .foregroundColor(.blue)
            ZStack{
                Color.blue
                    .edgesIgnoringSafeArea(.all)
                    .frame(width: 210, height: 210)
                    .cornerRadius(110)
                Image("tux")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 200,height: 200)
                    .background(Color.red)
                    .cornerRadius(100)
            }
            VStack {
                TextField("Ingresa tu nombre de usuario", text: $userText)
                    .textFieldStyle(.roundedBorder)
                TextField("Ingresa tu contraseña", text: $userPassText)
                    .textFieldStyle(.roundedBorder)
                //Text("El valor ingresado es: \(userText)")
            }
            .padding()
            Button {
                print("Login action...")
                print("user: \(userText)")
                print("pass: \(userPassText)")
            } label: {
                Text("Login")
                    .frame(width: 200, alignment: .center)
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.blue)
                    .cornerRadius(50)
            }
            HStack{
                Button("Registro") {
                    print("Registro action...")
                }
                Button("| Olvide mi contraseña"){
                    print("Recuperar contraseña...")
                }
            }
            HStack{
                Toggle(isOn: $estadoToggle) {
                    Text("Politica de uso:")
                        .font(.subheadline)
                        .foregroundColor(.gray)
                }
                .padding()
            }
            //Text("El estado del Toggle es: \(estadoToggle ? "Activo":"Desactivado")")
        }
    }
}

struct SliderContentView: View{
    
    @State var fontSize: CGFloat = 20.0
    
    var body: some View {
        VStack{
            Text("¡Cambia el tamaño de la fuente!")
                .font(.system(size: fontSize))
                .padding()
            Slider(value: $fontSize, in: 10...50, step:1)
                .padding()
        }
    }
}

struct StepperContentView: View{
    @State var itemCount: Int = 0
    var body: some View{
        VStack{
            Text("Carrito de compra")
                .font(.title)
                .padding(.top, 20)
            Spacer()
            Image(systemName: "cart")
                .resizable()
                .frame(width: 100,height: 100,alignment: .center)
            Spacer()
            Text("Cantidad: \(itemCount)")
                .font(.headline)
            Stepper {
                Text("Agregar al carrito")
            } onIncrement: {
                self.itemCount += 1
            } onDecrement: {
                if itemCount > 0 {
                    self.itemCount -= 1
                }
            }
            .padding()
            
            Button {
                print("TODO: Hacer compra")
            } label: {
                Text("Comprar")
                    .foregroundColor(.white)
            }
            .padding()
            .background(Color.green)
            .cornerRadius(10)
            Spacer()


        }
    }
}

struct ListContentView: View{
    
    var items = [
        Item(name: "Articulo 1", description: "Descripcion 1"),
        Item(name: "Articulo 2", description: "Descripcion 2"),
        Item(name: "Articulo 3", description: "Descripcion 3"),
        Item(name: "Articulo 4", description: "Descripcion 4"),
        Item(name: "Articulo 5", description: "Descripcion 5"),
        Item(name: "Articulo 6", description: "Descripcion 6"),
        Item(name: "Articulo 7", description: "Descripcion 7"),
        Item(name: "Articulo 8", description: "Descripcion 8"),
        Item(name: "Articulo 9", description: "Descripcion 9"),
        Item(name: "Articulo 10", description: "Descripcion 10")
    ]
    
    var body: some View{
        NavigationView {
            List(items){
                item in
                NavigationLink(destination: DetailView(item: item)) {
                    HStack {
                        Image(systemName: "newspaper")
                        Text(item.name)
                            .font(.subheadline)
                        .foregroundColor(.brown)
                    }
                }
            }
        }
    }
}

struct ScrollContentView: View{
    var body: some View{
        ScrollView([.horizontal, .vertical]){
                Image("colorful")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct TabContentView: View {
    @State var selectedTab = 2
    var body: some View{
        TabView(selection: $selectedTab) {
            Image("tux")
                .resizable()
                .scaledToFit()
                .tag(0)
                .tabItem {
                    Label("Tab 1", systemImage: "house")
                }
            VStack{
                Circle()
                    .foregroundColor(.blue)
                    .frame(width: 100, height: 100)
                Text("Este es el Tab 2")
            }
            .tag(1)
            .tabItem{
                Label("Tab 2", systemImage: "heart")
            }
            Text("This is Tab 3")
                .tag(2)
                .tabItem {
                    Label("Tab 3", systemImage: "map")
                }
        }
    }
}

struct AlertContentView: View{
    
    @State var showAlert = false
    
    var body: some View{
        VStack{
            Button("Show Alert"){
                self.showAlert = true
            }
        }
        .alert(isPresented: $showAlert) {
            Alert(title: Text("Titulo de la alerta"),
                  message: Text("Aqui va el mensaje de la alerta"),
                  primaryButton: .destructive(
                    Text("Delete"), action: {
                        print("Delete")
                    }), secondaryButton: .cancel(
                        Text("Cancelar"))
                  )
        }
    }
}

struct SheetContentView: View{
    
    @State var showAlert = false
    @State var selectedOption = 0
    
    var body: some View{
        VStack{
            Button("Show Sheet"){
                self.showAlert = true
            }
        }
        .actionSheet(isPresented: $showAlert) {
            ActionSheet(title: Text("Selecciona una opcion..."),
                        buttons: [
                            .default(Text("Opcion 1")){
                                self.selectedOption = 1
                                print("Se selecciono la opcion 1")
                            },
                            .default(Text("Opcion 2")){
                                self.selectedOption = 2
                                print("Se selecciono la opcion 2")
                            },
                            .cancel()
                        ]
            )
        }
    }
}

struct DateContentView:View{
    @State var selectedDate = Date()
    var body: some View{
        VStack{
            DatePicker("Seleccione un fecha: ", selection: $selectedDate, displayedComponents: [.date])
                .datePickerStyle(.wheel)
                .labelsHidden()
                .onChange(of: selectedDate) { newValue in
                    print(newValue)
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/YY"
                    print(formatter.string(from: newValue))
                }
            Text("La fecha actual es: \(selectedDate)")
                .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        DateContentView()
    }
}
